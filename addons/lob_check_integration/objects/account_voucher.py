######################################################################################################
# Improvements made to various aspects of the Sales modules
#
# Authors: Harry Alvarez, Dan Haggerty
# Date:    9/4/2015
######################################################################################################
import locale
from openerp.osv import fields, osv, orm
import logging
import lob

from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL,'en_US.utf8')

######################################################################################################
# Inherited sale order class definition
######################################################################################################
class account_voucher(osv.osv):

    _inherit = "account.voucher"

    def send_check_to_lob(self, cr, uid, ids, context={}):

        lob.api_key = 'your-api-key'

        lob.Check.create(
          description = 'Demo Check',
          to_address = {
            'name': 'Harry Zhang',
            'address_line1': '123 Test Street',
            'address_city': 'Mountain View',
            'address_state': 'CA',
            'address_zip': '94041',
            'address_country': 'US'
          },
          bank_account = 'bank_dfceb4a2a05b57e',
          amount = 2200,
          memo = 'rent',
          logo = 'https://s3-us-west-2.amazonaws.com/lob-assets/lob_check_logo.png',
          file = '<h1 style='padding-top:4in;'>Demo Check for {{name}}</h1>',
          data = {
            'name': 'Harry'
          }
        )

