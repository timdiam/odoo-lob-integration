{
    'name': 'Lob Check Integration',
    'version': '1.0',
    'description': """
    This module leverages the Lob API to mail checks from within the Odoo Interface   """,
    'author': 'Tim Diamond',
    'website': 'www.lob.com',
    "depends" : [
                'account',
                'account_check_writing',
                ],
    "data" : [
                'views/check_view.xml',
             ],
    "installable": True,
    "auto_install": False
}

